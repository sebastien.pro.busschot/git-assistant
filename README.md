# Git Assistant

A simple script for learning bash and make a crontab routine for git
## Installation

Copy the script on you want

```bash
git clone https://gitlab.com/sebastien.pro.busschot/git-assistant.git

# x-terminal-emulator : lauch the script on default system terminal, perfect for cron
x-terminal-emulator -e ./gitassistant.sh [PATH_WORK] [OPTION]
```

## Usage

```bash
# help
./gitassistant.sh -h
# launch menu on selected path
./gitassistant.sh /home/user/example
# launch selected action on selected path
./gitassistant.sh /home/user/example -i
```

## Menu
- -i git init / remote add / checkout -b master / pull master
- -r erase all in the directory / launch -i option
- -c add all and commit
- -p pull and push current branch
- -a loop on all branch and launch -p option
- -q exit the script

## Contributing
It's a learning project for bash, take it if it can help you and make all changes you want, errors is learning

## License
[MIT](https://choosealicense.com/licenses/mit/)