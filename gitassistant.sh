#!/bin/bash

echo "Bienvenue sur l'assistant GIT BeWeb"
if [[ $# -gt 1 ]]
then
    menu=$(echo $2 | cut -c2)
else
    menu="start"
fi

help(){
    echo "
    =========================SECTION AIDE=========================
    
    ./script [PATH] [OPTION]

    Le script se lance avec ou sans arguments.
    Le premier argument permet d'acceder à l'aide avec '-h'
        ou de spécifier le dossier de travail
    Le second permet de choisir l'action à effectuer sans
        passer par le menu

    Le script demande confirmation afin de s'assurer qu'il
     s'execute sur le bon dossier

    "
}

directory(){
    while :
    do
        pwd=`pwd`
        echo "Le script se lance dans le dossier : $pwd"
        read -p "Le dossier est-il correct? y/n " response
        if [[ "$response" == y ]]
            then
                cd $pwd
                break
        else
            read -p "Indiquez le répertoire de travail : " path
            cd $path
        fi
    done
}

gitInit(){
    git init
    git remote add origin $1
    git checkout -b master
    git pull origin master
}

install(){
    read -p "Veuillez indiquer sur quel dépot git travailler: " repository
    echo "Ajout de : $repository
    "
    gitInit $repository
}

reinstall(){
    directory
    oldRemote=$(git ls-remote --get-url)
    rm -rf * 2> /dev/null
    rm -rf .* 2> /dev/null
    gitInit $oldRemote
}

commit(){
    read -p "Quel message ajouter au commit ? " message
    date=$(date)
    message="$date $message"
    git add .
    git commit -m "$message"
}

push(){
    git pull origin $1
    git push origin $1
}

allPush(){
    branchOrigin=$(git branch | grep "*" | cut -c3-)
    branches=$(git branch)
    for branch in $branches
    do
        if [[ !(-f $branch) && !(-d $branch) ]] 
        then
            git checkout $branch
            push $branch
        fi
    done
    git checkout $branchOrigin
}

if [[ -z $1 ]] 
then
    directory
elif [[ "$1" = "-h" ]] 
then
    help
    directory
else
 cd $1
fi


while :
do
    case $menu in
        "q")
            break
        ;;
        "i")
            install
        ;;
        "r")
            reinstall
        ;;
        "c")
            commit
        ;;
        "p")
            branch=$(git branch | grep "*" | cut -c3-)
            push $branch
        ;;
        "a")
            allPush
        ;;
        "start");;
        *)
            echo "$menu n'est pas une option valide"
        ;;
    esac
    echo "
    Choisisez une option:
     'i') pour initialiser git et se connecter au dépot
     'r') pour réinitialiser git et se connecter au dépot,
     Attention ! contient rm -rf * et rm -rf .*
     'c') pour réaliser un commit
     'p') mettre a jour la branche distante
     'a') mettre à jour toutes les branches
     'q') pour quitter
     "
    read -p "Votre choix : " menu
done